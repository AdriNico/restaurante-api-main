# restaurante-api-main

### Introducción

Es la base que construímos en el webinar Sprint Project 01 de la carrera de desarrollo web back end de Acámica

### Instrucciones de instalación


git clone https://gitlab.com/AdriNico/restaurante-api-main
cd restaurante-api-main
npm install
iniciar el Xampp (Con Mysql Y Apache corriendo)
Exportar desde la carpeta Data la Bd (restaurante_main.sql)
Cambiar el nombre de example.env a .env para que coincida donde se lo requiera
npm start