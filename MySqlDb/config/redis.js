const redis = require('redis')
const bluebird = require('bluebird');
bluebird.promisifyAll(redis);

const redisClient = redis.createClient({
    host: '127.0.0.1',
    port: 6379
});

redisClient.on('error', (err) => {
    console.error(err);
});

module.exports = redisClient;
