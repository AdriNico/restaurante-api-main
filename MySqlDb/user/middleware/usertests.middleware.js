require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../../config/db.config");
const userModel = require('../models/user.model')(connection, Sequelize);

async function email(req, res, next) {
    const email = await userModel.findOne({where : {email_user : req.body.email_user}})
    if (email){
        return res.json("Usuario registrado")
    }else{
        next();
    }
}

module.exports = email;