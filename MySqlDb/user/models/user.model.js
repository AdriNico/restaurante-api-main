const usermodel = (connection, Sequelize) => {
    const user = connection.define('user', {
        id_user: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        nombre_user: {
            type: Sequelize.STRING
        },
        contrasenia_user: {
            type: Sequelize.STRING
        },
        email_user: {
            type: Sequelize.STRING
        },
        username_user: {
            type: Sequelize.STRING
        },
        telefono_user: {
            type: Sequelize.BIGINT 
        },
        direccion_user: {
            type: Sequelize.STRING
        },
        id_rol: {
            type: Sequelize.INTEGER,
            references: 'roles',
            referencesKey: 'id_rol'
        }
        
    },
    {
        timestamps: false
    });
    return user
}



module.exports = usermodel;
