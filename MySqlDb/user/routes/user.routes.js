require('dotenv').config();
const Sequelize = require('sequelize');
const connection = require("../../config/db.config");
const jwt = require('jsonwebtoken');
const express = require('express')
const router = express.Router()
const userModel = require('../models/user.model')(connection, Sequelize);
const userController = require("../controllers/user.controllers");
const email = require("../middleware/usertests.middleware")

/**
 * @swagger
 * /api/v1/user:
 *  post:
 *    description: crea un usuario
 *    parameters:
 *    - name: id_user
 *      description: Id del usuario
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre_user
 *      description: nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: contrasenia_user
 *      description: contrasenia del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email_user
 *      description: email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *      format: email
 *    - name: username_user
 *      description: username del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: telefono_user
 *      description: telefono del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: direccion_user
 *      description: direccion del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: id_rol
 *      description: tipo del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.post("/", email, async (req, res) => {
        userController.createUser(req)
            .then(() => {
                res.status(200).send({
                    status: 200,
                    message: "Data Save Successfully",
                });
            })
            .catch(error => {
                res.status(400).send({
                    message: "Unable to insert data",
                    errors: error,
                    status: 400
                });
            });
    }
);

/**
 * @swagger
 * /api/v1/user:
 *  get:
 *    description: lista todos los usuarios
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            userController.listUser()
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/user/{id}:
 *  put:
 *    description: actualiza un usuario por id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id
 *      description: Id del usuario
 *      in: path
 *      required: false
 *      type: integer
 *    - name: nombre_user
 *      description: nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: contrasenia_user
 *      description: contrasenia del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email_user
 *      description: email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *      format: email
 *    - name: username_user
 *      description: username del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: telefono_user
 *      description: telefono del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: direccion_user
 *      description: direccion del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: id_rol
 *      description: tipo del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.put("/:id", async (req, res) => {
    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            userController.updateUser(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Update Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Update data",
                        errors: error,
                        status: 400
                    });
                });
        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/user/{id}:
 *  delete:
 *    description: elimina un usuario de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del usuario
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.delete("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            userController.deleteUser(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Delete Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Delete data",
                        errors: error,
                        status: 400
                    });
                });
        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }
});

/**
 * @swagger
 * /api/v1/user/{id}:
 *  get:
 *    description: busca un usuario de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del usuario
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/:id", async (req, res) => {
    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            userController.listUserById(req)
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }
});

const login = async (req, res) => {
    userModel.findOne({ where: { nombre_user: req.body.username } })
        .then(resolve => {
            if (req.body.password == resolve.contrasenia_user) {
                let token = userController.createToken(resolve)
                res.send(token);
            } else {
                res.send('Error, datos incorrectos')
            }
        })
        .catch(err => res.send('Error, datos incorrectos'))
}

/**
 * @swagger
 * /api/v1/user/login:
 *  post:
 *    description: Logea un usuario
 *    parameters:
 *    - name: username
 *      description: nombre del usuario
 *      in: formData
 *      required: false
 *      type: string
 *    - name: password
 *      description: contrasenia del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.post('/login', (req, res) => {
    const username = req.body.username;
    console.log(req.body)
    console.log(`username: ${username}`);
    const password = req.body.password;
    console.log(`password: ${password}`);
    //es lo que esta adentro
    login(req, res)
});

module.exports = router;