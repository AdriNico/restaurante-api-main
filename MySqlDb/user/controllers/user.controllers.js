require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../../config/db.config");
const userModel = require('../models/user.model')(connection, Sequelize);
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const createToken = (user) => {
    let payload = {
        userId: user.id_user,
        id_rol: user.id_rol
    }
    return jwt.sign(payload, process.env.TOKEN_KEY, { expiresIn: '24h' });
};

const consumeToken = (token) => {
    try {
        console.log(process.env.TOKEN_KEY);
        let cleantoken = token;
        cleantoken = cleantoken.replace('Bearer ', '');
        const detoken = jwt.verify(cleantoken, process.env.TOKEN_KEY);
        return detoken;
    } catch (error) {
        console.log(error);
        return false;
    }
}

const pass = async (contrasenia_user) => await bcrypt.hash(contrasenia_user, 8);

const createUser = async (req) => {
    //let pass = pass(req.contrasenia_user);

    const newUser = await userModel.build({
        nombre_user: req.body.nombre_user,
        contrasenia_user: req.body.contrasenia_user,
        id_user: req.body.id_user,
        email_user: req.body.email_user,
        username_user: req.body.username_user,
        telefono_user: req.body.telefono_user,
        direccion_user: req.body.direccion_user,
        id_rol: req.body.id_rol
    });
    const result = await newUser.save();
    return result;
}


const listUser = async () => await userModel.findAll();

const updateUser = async (req) => {
    const userId = parseInt(req.params.id);
    const result = await userModel.update({
        nombre_user: req.body.nombre_user,
        contrasenia_user: req.body.contrasenia_user,
        id_user: req.body.id_user,
        email_user: req.body.email_user,
        username_user: req.body.username_user,
        telefono_user: req.body.telefono_user,
        direccion_user: req.body.direccion_user,
        id_rol: req.body.id_rol
    },
        { where: { id_user: userId } }
    ).then((result) => {
        if (!result) {
            throw "Usuario no existente"
        }
    });
    return result;
}

const deleteUser = async (req) => {
    const userId = req.params.id;
    const result = await userModel.destroy({
        where: { id_user: userId }
    }).then((result) => {
        if (!result) {
            throw "Usuario no existente"
        }
    });

    return result;
}

const listUserById = async (req) => {
    const userId = req.params.id;
    const result = await userModel.findOne({
        where: { id_user: userId }
    }).then((result) => {
        if (!result) {
            throw "Usuario no existente"
        }
    })
    return result;
}

const getByEmail = async (email_user) => {
    const resultado = await userModel.findOne({ where: email_user });
    console.log(resultado);
};

const searchUser = async (username) => {
    const usuarioEncontrado = await userModel.findOne({ where: { nickname: username } });
    return usuarioEncontrado;
}


module.exports = {
    createUser,
    listUser,
    updateUser,
    deleteUser,
    listUserById,
    getByEmail,
    searchUser,
    createToken,
    consumeToken
}
