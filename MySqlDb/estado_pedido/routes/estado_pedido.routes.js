require('dotenv').config();
const express = require('express')
const router = express.Router()
const estado_pedidoController = require("../controllers/estado_pedido.controllers");
const userController = require("../../user/controllers/user.controllers");

/**
 * @swagger
 * /api/v1/estado_pedido:
 *  post:
 *    description: crea un estado de pedidos
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id_estado_pedido
 *      description: Id del estado del pedido
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre_estado_pedido
 *      description: nombre del estado del pedido
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.post("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            estado_pedidoController.createEstado_pedido(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Save Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to insert data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/estado_pedido:
 *  get:
 *    description: lista todos los estados del pedido
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            estado_pedidoController.listEstado_pedido()
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/estado_pedido/{id}:
 *  put:
 *    description: actualiza un estado de pedido por id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id
 *      description: Id del estado del pedido
 *      in: path
 *      required: false
 *      type: integer
 *    - name: nombre_estado_pedido
 *      description: nombre del estado del pedido
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.put("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            estado_pedidoController.updateEstado_pedido(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Update Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Update data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/estado_pedido/{id}:
 *  delete:
 *    description: elimina un estado de pedido de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del estado de pedido
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.delete("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            estado_pedidoController.deleteEstado_pedido(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Delete Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Delete data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/estado_pedido/{id}:
 *  get:
 *    description: busca un estado de pedido de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del estadod de pedido
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            estado_pedidoController.listEstado_pedidoById(req)
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

module.exports = router;