const estado_pedidomodel = (connection, Sequelize) => {
    const estado_pedido = connection.define('estado_pedido', {
        id_estado_pedido: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        nombre_estado_pedido: {
            type: Sequelize.STRING
        }   
    },
    {
        timestamps: false
    });
    return estado_pedido
}

module.exports = estado_pedidomodel;
