require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../../config/db.config");
const estado_pedidoModel = require('../models/estado_pedido.models')(connection, Sequelize);

const createEstado_pedido = async (req) => {
    const newEstado_pedido = await estado_pedidoModel.build({
        id_estado_pedido: req.body.id_estado_pedido,
        nombre_estado_pedido: req.body.nombre_estado_pedido
    });

    const result = await newEstado_pedido.save();
    return result;
}

const listEstado_pedido = async () => await estado_pedidoModel.findAll();

const updateEstado_pedido = async (req) => {
    const estado_pedidoId = parseInt(req.params.id);
    const result = await estado_pedidoModel.update({
        id_estado_pedido: req.body.id_estado_pedido,
        nombre_estado_pedido: req.body.nombre_estado_pedido
    },
        { where: { id_estado_pedido : estado_pedidoId } }  
    ).then((result) => {
        if (!result) {
            throw "Estado de pedido no existente"
        }
    });
    return result;
}

const deleteEstado_pedido = async (req) => {
    const estado_pedidoId = req.params.id;
    const result = await estado_pedidoModel.destroy({
        where: { id_estado_pedido: estado_pedidoId }
    }).then((result) => {
        if (!result) {
            throw "Estado de pedido no existente"
        }
    });
    return result;
}

const listEstado_pedidoById = async (req) => {
    const estado_pedidoId = req.params.id;
    const result = await estado_pedidoModel.findOne({
        where: { id_estado_pedido: estado_pedidoId }
    }).then((result) => {
        if (!result) {
            throw "Estado de pedido no existente"
        }
    });
    return result;
}

module.exports = {
    createEstado_pedido,
    listEstado_pedido,
    updateEstado_pedido,
    deleteEstado_pedido,
    listEstado_pedidoById
}