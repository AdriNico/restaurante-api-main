require('dotenv').config();
const express = require('express')
const router = express.Router()
const pedidoController = require("../controllers/pedido.controllers");
const userController = require("../../user/controllers/user.controllers");

/**
 * @swagger
 * /api/v1/pedido:
 *  post:
 *    description: crea un pedido
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id_pedido
 *      description: Id del pedido
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: id_user
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: id_medios_de_pago
 *      description: medios de pago disponibles
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: id_estado_pedido
 *      description: Id del estado del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: valor_pedido
 *      description: valor del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: fecha_creacion_pedido
 *      description: fecha de la creacion del pedido
 *      in: formData
 *      required: false
 *      type: date
 *    - name: direccion_pedido
 *      description: direccion de destino del pedido
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.post("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {

        pedidoController.createPedido(req)
            .then(() => {
                res.status(200).send({
                    status: 200,
                    message: "Data Save Successfully",
                });
            })
            .catch(error => {
                res.status(400).send({
                    message: "Unable to insert data",
                    errors: error,
                    status: 400
                });
            });


    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/pedido:
 *  get:
 *    description: lista todos los pedidos
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            pedidoController.listPedido()
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/pedido/{id}:
 *  put:
 *    description: actualiza un pedido por id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id
 *      description: Id del pedido
 *      in: path
 *      required: false
 *      type: integer
 *    - name: id_user
 *      description: Id del usuario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: id_medios_de_pago
 *      description: medios de pago disponibles
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: id_estado_pedido
 *      description: Id del estado del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: valor_pedido
 *      description: valor del pedido
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: fecha_creacion_pedido
 *      description: fecha de la creacion del pedido
 *      in: formData
 *      required: false
 *      type: date
 *    - name: direccion_pedido
 *      description: direccion de destino del pedido
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.put("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            pedidoController.updatePedido(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Update Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Update data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/pedido/{id}:
 *  delete:
 *    description: elimina un pedido de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del pedido
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.delete("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            pedidoController.deletePedido(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Delete Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Delete data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/pedido/{id}:
 *  get:
 *    description: busca un pedido de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del pedido
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            pedidoController.listPedidoById(req)
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

module.exports = router;