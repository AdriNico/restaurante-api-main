require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../../config/db.config");
const pedidoModel = require('../models/pedido.models')(connection, Sequelize);

const createPedido = async (req) => {
    const newPedido = await pedidoModel.build({
        id_pedido: req.body.id_pedido,
        id_user: req.body.id_user,
        id_medios_de_pago: req.body.id_medios_de_pago,
        id_estado_pedido: req.body.id_estado_pedido,
        valor_pedido: req.body.valor_pedido,
        fecha_pedido: req.body.fecha_pedido,
        direccion_pedido: req.body.direccion_pedido
    });

    const result = await newPedido.save();
    return result;
}

const listPedido = async () => await pedidoModel.findAll();

const updatePedido = async (req) => {
    const pedidoId = parseInt(req.params.id);
    const result = await pedidoModel.update({
        id_pedido: req.body.id_pedido,
        id_user: req.body.id_user,
        id_medios_de_pago: req.body.id_medios_de_pago,
        id_estado_pedido: req.body.id_estado_pedido,
        valor_pedido: req.body.valor_pedido,
        fecha_pedido: req.body.fecha_pedido,
        direccion_pedido: req.body.direccion_pedido
    },
        { where: { id_pedido: pedidoId } }
    ).then((result) => {
        if (!result) {
            throw "Pedido no existente"
        }
    });
    return result;
}

const deletePedido = async (req) => {
    const pedidoId = req.params.id;
    const result = await pedidoModel.destroy({
        where: { id_pedido: pedidoId }
    }).then((result) => {
        if (!result) {
            throw "Pedido no existente"
        }
    });
    return result;
}

const listPedidoById = async (req) => {
    const pedidoId = req.params.id;
    const result = await pedidoModel.findOne({
        where: { id_pedido: pedidoId }
    }).then((result) => {
        if (!result) {
            throw "Pedido no existente"
        }
    });
    return result;
}

module.exports = {
    createPedido,
    listPedido,
    updatePedido,
    deletePedido,
    listPedidoById
}