const pedidomodel = (connection, Sequelize) => {
    const pedido = connection.define('pedido', {
        id_pedido: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },  
        id_user: {
            type: Sequelize.INTEGER,
            references: 'user',
            referencesKey: 'id_user'
        },
        id_medios_de_pago: {
            type: Sequelize.INTEGER,
            references: 'medios_de_pago',
            referencesKey: 'id_medios_de_pago'
        },
        id_estado_pedido: {
            type: Sequelize.INTEGER,
            references: 'estado_pedido',
            referencesKey: 'id_estado_pedido'
        },
        valor_pedido: {
            type: Sequelize.INTEGER
        },
        fecha_creacion_pedido: {
            type: Sequelize.DATE
        },
        direccion_pedido: {
            type: Sequelize.STRING
        }
        
    },
    {
        timestamps: false
    });
    return pedido
}

module.exports = pedidomodel;
