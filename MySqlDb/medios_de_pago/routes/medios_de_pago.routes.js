require('dotenv').config();
const express = require('express')
const router = express.Router();
const medios_de_pagoController = require("../controllers/medios_de_pago.controllers");
const userController = require("../../user/controllers/user.controllers");

/**
 * @swagger
 * /api/v1/medios_de_pago:
 *  post:
 *    description: crea un medio de pago
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id_medios_de_pago
 *      description: Id del medio de pago
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre_medios_de_pago
 *      description: nombre del medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.post("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            medios_de_pagoController.createMedios_de_pago(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Save Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to insert data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/medios_de_pago:
 *  get:
 *    description: lista todos los medios de pago
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {



        medios_de_pagoController.listMedios_de_pago()
            .then((result) => {
                res.status(200).send({
                    status: 200,
                    message: "Data find Successfully",
                    data: result
                });
            })
            .catch(error => {
                res.status(400).send({
                    message: "Unable to find data",
                    errors: error,
                    status: 400
                });
            });

    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/medios_de_pago/{id}:
 *  put:
 *    description: actualiza un medio de pago por id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id
 *      description: Id del medio de pago
 *      in: path 
 *      required: false
 *      type: integer
 *    - name: nombre_medios_de_pago
 *      description: nombre del medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.put("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            medios_de_pagoController.updateMedios_de_pago(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Update Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Update data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/medios_de_pago/{id}:
 *  delete:
 *    description: elimina un medio de pago de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del medio de pago
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.delete("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            medios_de_pagoController.deleteMedios_de_pago(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Delete Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Delete data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/medios_de_pago/{id}:
 *  get:
 *    description: busca un medios de pago de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del medio de pago
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            medios_de_pagoController.listMedios_de_pagoById(req)
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

module.exports = router;