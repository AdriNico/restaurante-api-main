const medios_de_pagomodel = (connection, Sequelize) => {
    const medios_de_pago = connection.define('medios_de_pago', {
        id_medios_de_pago: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        nombre_medios_de_pago: {
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    });
    return medios_de_pago;
}

module.exports = medios_de_pagomodel;