require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../../config/db.config");
const medios_de_pagoModel = require('../models/medios_de_pago.models')(connection, Sequelize);


const createMedios_de_pago = async (req) => {
    const newMedios_de_pago = await medios_de_pagoModel.build({
        id_medios_de_pago: req.body.id_medios_de_pago,
        nombre_medios_de_pago: req.body.nombre_medios_de_pago
    });

    const result = await newMedios_de_pago.save();
    return result;
}

const listMedios_de_pago = async () => await medios_de_pagoModel.findAll();

const updateMedios_de_pago = async (req) => {
    const medios_de_pagoId = parseInt(req.params.id);
    const result = await medios_de_pagoModel.update({
        id_medios_de_pago: req.body.id_medios_de_pago,
        nombre_medios_de_pago: req.body.nombre_medios_de_pago
    },
        { where: { id_medios_de_pago: medios_de_pagoId } }
    ).then((result) => {
        if (!result) {
            throw "Medio de pago no existente"
        }
    });
    return result;
}

const deleteMedios_de_pago = async (req) => {
    const medios_de_pagoId = req.params.id;
    const result = await medios_de_pagoModel.destroy({
        where: { id_medios_de_pago: medios_de_pagoId }
    }).then((result) => {
        if (!result) {
            throw "Medio de pago no existente"
        }
    });
    return result;
}

const listMedios_de_pagoById = async (req) => {
    const medios_de_pagoId = req.params.id;
    const result = await medios_de_pagoModel.findOne({
        where: { id_medios_de_pago: medios_de_pagoId }
    }).then((result) => {
        if (!result) {
            throw "Medio de pago no existente"
        }
    });
    return result;
}

module.exports = {
    createMedios_de_pago,
    listMedios_de_pago,
    updateMedios_de_pago,
    deleteMedios_de_pago,
    listMedios_de_pagoById
}

