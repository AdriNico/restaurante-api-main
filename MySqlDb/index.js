require('dotenv').config();
const Sequelize = require('sequelize');
const express = require("express");
const helmet = require("helmet");
const connection = require("./config/db.config");
const app = express();
const PORT = process.env.PORT || 3030;
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const productRoutes = require("./product/routes/product.routes");
const userRoutes = require("./user/routes/user.routes");
const medios_de_pagoRoutes = require("./medios_de_pago/routes/medios_de_pago.routes");
const estado_pedidoRoutes = require("./estado_pedido/routes/estado_pedido.routes");
const pedidoRoutes = require("./pedido/routes/pedido.routes");
const rolesRoutes = require("./roles/routes/roles.routes");

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'API de restaurante',
            version: '2.0.0'
        }
    },
    apis: ['./MySqlDb/index.js',
        './MySqlDb/user/routes/user.routes.js',
        './MySqlDb/estado_pedido/routes/estado_pedido.routes.js',
        './MySqlDb/medios_de_pago/routes/medios_de_pago.routes.js',
        './MySqlDb/pedido/routes/pedido.routes.js',
        './MySqlDb/product/routes/product.routes.js',
        './MySqlDb/roles/routes/roles.routes.js'
    ],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(helmet());

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));

app.use('/api/v1/estado_pedido', estado_pedidoRoutes);
app.use('/api/v1/medios_de_pago', medios_de_pagoRoutes);
app.use('/api/v1/pedido', pedidoRoutes);
app.use('/api/v1/product', productRoutes);
app.use('/api/v1/roles', rolesRoutes);
app.use('/api/v1/user', userRoutes);

app.listen(PORT, () => {
    console.log(`Server is running on port http://localhost:${PORT}`);
    connection.authenticate().then(() => {
        console.log('BBDD conectada');
    }).catch((err) => {
        console.log('BBDD error', err);
    })
});
