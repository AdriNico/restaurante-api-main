require('dotenv').config();
const express = require('express')
const router = express.Router()
const rolesController = require("../controllers/roles.controllers");
const userController = require("../../user/controllers/user.controllers");

/**
 * @swagger
 * /api/v1/roles:
 *  post:
 *    description: crea un rol
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id_rol
 *      description: Id del rol
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre_rol
 *      description: nombre del rol
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.post("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            rolesController.createRoles(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Save Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to insert data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/roles:
 *  get:
 *    description: lista todos los roles
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            rolesController.listRoles()
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });


        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/roles/{id}:
 *  put:
 *    description: actualiza un rol por id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id
 *      description: Id del rol
 *      in: path
 *      required: false
 *      type: integer
 *    - name: nombre_rol
 *      description: nombre del rol
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.put("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            rolesController.updateRoles(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Update Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Update data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json
    }

});

/**
 * @swagger
 * /api/v1/roles/{id}:
 *  delete:
 *    description: elimina un rol de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del rol
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.delete("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            rolesController.deleteRoles(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Delete Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Delete data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/roles/{id}:
 *  get:
 *    description: busca un rol de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del rol
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */


router.get("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            rolesController.listRolesById(req)
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

module.exports = router;