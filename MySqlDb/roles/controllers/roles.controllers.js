require("dotenv").config();
const Sequelize = require('sequelize');
const connection = require("../../config/db.config");
const rolesModel = require('../models/roles.model')(connection, Sequelize);

const createRoles = async (req) => {
    const newRoles = await rolesModel.build({
        id_rol: req.body.id_rol,
        nombre_rol: req.body.nombre_rol
    });

    const result = await newRoles.save();
    return result;
}

const listRoles = async () => await rolesModel.findAll();

const updateRoles = async (req) => {
    const rolesId = parseInt(req.params.id);
    const result = await rolesModel.update({
        id_rol: req.body.id_rol,
        nombre_rol: req.body.nombre_rol
    },
        { where: {id_rol : rolesId}}
    ).then((result) => {
        if (!result) {
            throw "Rol no existente"
        }
    });
    return result;
}

const deleteRoles = async (req) => {
    const rolesId = req.params.id;
    const result = await rolesModel.destroy({
        where: { id_rol: rolesId }
    }).then((result) => {
        if (!result) {
            throw "Rol no existente"
        }
    });
    return result;
}

const listRolesById = async (req) => {
    const rolesId = req.params.id;
    const result = await rolesModel.findOne({
        where : { id_rol: rolesId} 
    }).then((result) => {
        if (!result) {
            throw "Rol no existente"
        }
    });
    return result;
}

module.exports = {
    createRoles,
    listRoles,
    updateRoles,
    deleteRoles,
    listRolesById
}