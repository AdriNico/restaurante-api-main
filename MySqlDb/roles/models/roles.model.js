const rolesmodel = (connection, Sequelize) => {
    const roles = connection.define('roles', {
        id_rol: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        nombre_rol: {
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    });
    return roles
}

module.exports = rolesmodel;