const express = require('express')
const router = express.Router()
const userController = require("../../user/controllers/user.controllers");
const productController = require("../controllers/product.controllers");

/**
 * @swagger
 * /api/v1/product:
 *  post:
 *    description: crea un producto
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id_product
 *      description: Id del producto
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre_product
 *      description: nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: precio_product
 *      description: precio del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.post("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            productController.createProduct(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Save Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to insert data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }
});

/**
 * @swagger
 * /api/v1/product:
 *  get:
 *    description: lista todos los productos
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {


        productController.listProduct()
            .then((result) => {
                res.status(200).send({
                    status: 200,
                    message: "Data find Successfully",
                    data: result
                });
            })
            .catch(error => {
                console.log(error);
                res.status(400).send({
                    message: "Unable to find data",
                    errors: error,
                    status: 400
                });
            });

    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/product/{id}:
 *  put:
 *    description: actualiza un producto por id
 *    parameters:  
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *      type: string
 *    - name: id
 *      description: Id del producto
 *      in: path
 *      required: false
 *      type: integer
 *    - name: nombre_product
 *      description: nombre del producto
 *      in: formData
 *      required: true
 *      type: string
 *    - name: precio_product
 *      description: precio del producto
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */

router.put("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            productController.updateProduct(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Update Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Update data",
                        errors: error,
                        status: 400
                    });
                });
        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/product/{id}:
 *  delete:
 *    description: elimina un producto de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del producto
 *      in: path
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.delete("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {

            productController.deleteProduct(req)
                .then(() => {
                    res.status(200).send({
                        status: 200,
                        message: "Data Delete Successfully",
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to Delete data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

/**
 * @swagger
 * /api/v1/product/{id}:
 *  get:
 *    description: busca un producto de acuerdo a su id
 *    parameters:
 *    - name: authorization
 *      description: User Token
 *      in: header
 *      required: true
 *    - name: id
 *      description: Id del producto
 *      in: path      
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */

router.get("/:id", async (req, res) => {

    let desToken = await userController.consumeToken(req.headers.authorization);
    if (desToken != false) {
        if (desToken.id_rol == 1) {


            productController.listProductById(req)
                .then((result) => {
                    res.status(200).send({
                        status: 200,
                        message: "Data find Successfully",
                        data: result
                    });
                })
                .catch(error => {
                    res.status(400).send({
                        message: "Unable to find data",
                        errors: error,
                        status: 400
                    });
                });

        } else {
            res.json('No tienes permisos')
        }
    } else {
        res.json('Usuario No logeado')
    }

});

module.exports = router;