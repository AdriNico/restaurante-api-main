require("dotenv").config();
const fetch = require('node-fetch');
//const client = require('../../config/redis');
const redis = require('redis');
const bluebird = require('bluebird');
bluebird.promisifyAll(redis);
const client = redis.createClient({
    host: process.env.ELASTIC_CACHE,
    port: 6379
})
const jwt = require('jsonwebtoken');
const connection = require("../../config/db.config");
const Sequelize = require('sequelize');
const productModel = require('../models/product.model')(connection, Sequelize);


const createProduct = async (req) => {
    const newProduct = await productModel.build({
        nombre_product: req.body.nombre_product,
        precio_product: req.body.precio_product,
        id_product: req.body.id_product
    });

    const result = await newProduct.save();
    return result;
}

const listProduct = async () => {

    const productOnRedis = await client.getAsync("productModel");

    if (productOnRedis !== null) {
        return JSON.parse(productOnRedis);
    } else {
        const result = await productModel.findAll()
            .then((res) => {
                client.set("productModel", JSON.stringify(res), 'EX', 60 * 20);
                return res;
            })
            .catch((err) => {
                return err
            });
        return result;
    }
}



const updateProduct = async (req) => {
    const productId = parseInt(req.params.id);
    const result = await productModel.update({
        nombre_product: req.body.nombre_product,
        precio_product: req.body.precio_product,
        id_product: req.body.id_product
    },
        {
            where: { id_product: productId }
        }).then((result) => {
            if (!result) {
                throw "Producto no existente"
            }
        });
    return result;
}

const deleteProduct = async (req) => {
    const productId = req.params.id;
    const result = await productModel.destroy({
        where: { id_product: productId }
    }).then((result) => {
        if (!result) {
            throw "Producto no existente"
        }
    });
    return result;
}

const listProductById = async (req) => {
    const productId = req.params.id;
    const result = await productModel.findOne({
        where: { id_product: productId }
    }).then((result) => {
            if (!result) {
                throw "Producto no existente"
            }
        });
    return result;
}

module.exports = {
    createProduct,
    listProduct,
    updateProduct,
    deleteProduct,
    listProductById
}
