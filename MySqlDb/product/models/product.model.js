const productmodel = (connection, Sequelize) => {
    const product = connection.define('product', {
        id_product: {
          type: Sequelize.INTEGER,
          primaryKey: true
        },
        nombre_product: {
          type: Sequelize.STRING
        },
        precio_product: {
          type: Sequelize.INTEGER
        } 
    },
    {
      timestamps: false
    });
    return product
}

module.exports = productmodel;
