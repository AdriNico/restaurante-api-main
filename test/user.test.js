const assert = require('assert');
const fetch = require('node-fetch');

const userRegister = 'http://localhost:3030/api/v1/user'
const fakeRegister = 'http://localhost:3030/api/v1/useer'

describe(`Pruebas userRegister`, () => {
  it(`user responde con un 200`, async () => {
    await fetch(userRegister)
      .then(response => {
        assert.equal(response.status, 200);
      })
  });



  it(`userRegister responde con un 404`, async () => {
    await fetch(fakeRegister)
      .then(response => {
        assert.equal(response.status, 404);
      });
  });

});

